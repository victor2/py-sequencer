# README #

Steps to get the Python Hyper Swing Sequencer up and running.

### What is this repository for? ###

* Quick summary

The application turns [AKAI MIDIMIX](https://www.akaipro.com/midimix) controller into an 32-step MIDI sequencer
with unlimited number of MIDI instruments tp control.
The controller has four rows with 8 controls in each. Each step has four parameters.

1. The upper row controls define note pitch
1. The second row controls define note duration
1. The thrid row controls define pauses
1. The last row controls note velocity 

* Version

December, 21st, 2020 

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

[Victor X](http://victorx.eu)

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
