import tkinter as tk
import mido
import time
import datetime
import random

sync = 'USB2.0-MIDI'
# outs = ['01. Internal', 'USB']
# channels = [[0, 1, 2], [1]]
outs = ['USB']
channels = [[0, 1]]
ranges = [{"low": 88, "high": 118}, {"low": 45, "high": 75}]

# the range of master values is 127 as it comes from MIDI
# the maximum (fastest) value is
master_range_max = 137
# the minimum (slowest) value
master_range_min = master_range_max - 127

# initial is half of the fastest 
master = master_range_max // 2

# default value corresponds to the minimum master
timeline_base = 2.0
# total duration of all slots, sec
timeline = timeline_base * master / master_range_min

window = None
canvas: tk.Canvas = None
# labels = []
slots = 6
# notes = []
note_values = []
titles = []
rates = []
midiIn = None
midiSync = None
midiOuts = []
midi_names = []
stopped = None
shifted = 0
pointers = []

current_slot = 0
current_out = 0

alt_mode = False
alt_mode2 = False
pause = True

last_clock = 0
update_diagram = True


def get_midi_input(name):
    for n in mido.get_input_names():
        # print(n)
        if name in n:
            return mido.open_input(n)


def get_midi_output(name):
    for n in mido.get_output_names():
        print(n)
        if name in n:
            return mido.open_output(n)


def init_midi():
    global midiIn
    global midiSync
    global midiOuts
    midiIn = get_midi_input('MIDI Mix')
    print(midiIn)

    midiSync = get_midi_input(sync)
    print('SYNC ', midiSync)

    for o in outs:
        out = get_midi_output(o)
        if out:
            midiOuts.append(out)
            print(out.name)

    if not len(midiOuts):
        print('No configured MIDI outputs detected. Adding default one.')
        out = get_midi_output('Microsoft')
        if out:
            midiOuts.append(out)
    print(midiOuts)
    return len(midiOuts)


def init_channels(num_outs):
    total = 0
    if num_outs >= len(channels):
        num_outs = len(channels)
    for idx in range(num_outs):
        total += len(channels[idx])
        for i in range(len(channels[idx])):
            rates.append(100)
            pointers.append(0)

    return total


def select_block(slot, out):
    global current_slot
    global current_out
    current_slot = slot
    current_out = out
    update_rates()


def apply_range(no, row):
    limits = ranges[row]
    low = limits['low']
    high = limits['high']
    return int(low + (high - low) / 127 * no)


# The 'first' is the same as alt mode - values are copied to all instruments
# and all time slots
# The 'alt mode 2' copies values to all time slots of the selected instrument
def store_value(val, row, col, first, alt2):
    # global notes
    global note_values
    numeric = int(val)
    if row % 4 == 0:
        numeric = apply_range(numeric, current_out)

    if alt2:
        for s in range(slots):
            c = col + s * 8
            # notes[row + current_out * 4][c + 1]["text"] = val
            note_values[row + current_out * 4][c] = numeric
    elif first:
        for s in range(slots):
            for o in range(len(note_values) // 4):
                r = row + o * 4
                c = col + s * 8
                #                print(r, c)
                # notes[r][c + 1]["text"] = val
                if row % 4 == 0:
                    numeric = apply_range(int(val), o)
                note_values[r][c] = numeric
    else:
        r = row + current_out * 4
        c = col + current_slot * 8
        # notes[r][c + 1]["text"] = val
        note_values[r][c] = numeric


def toggle_pause():
    global pause
    if pause:
        window.after(10, step_all)
    pause = not pause


def update_rates():
    # the sum of all durations and pauses in a given channel
    # averaged per slot
    global rates

    canvas.delete('main')
    rows = len(note_values) // 4
    gap = 10
    width = canvas.winfo_reqwidth() - gap
    height = canvas.winfo_reqheight() - gap
    ht = height // rows
    height = ht * rows
    wd = width // slots
    width = wd * slots
    h0 = 0
    scale_y = (ht - gap - 1) / 127.0
    fill = 'yellow'
    if alt_mode:
        fill = 'lightblue'
    canvas.create_rectangle(3, 1, width, height, fill=fill, width=1, tag='main')

    if not alt_mode:
        if alt_mode2:
            canvas.create_rectangle(3, ht * current_out + 1, width, ht * (current_out + 1), fill='lightblue', width=1, tag='main')
        else:
            canvas.create_rectangle(wd * current_slot + 3, ht * current_out + 1, wd * (current_slot + 1),
                                    ht * (current_out + 1), fill='lightblue', width=1, tag='main')

    for r in range(rows):
        r4 = r * 4
        summa = 0
        pts = [[], [0]]
        a = []
        n = []
        for s in range(slots):
            s8 = s * 8
            for c in range(8):
                n.append(note_values[r4 + 0][c + s8])
                a.append(note_values[r4 + 3][c + s8])
                for rr in range(1, 3):
                    summa += note_values[r4 + rr][c + s8]
                    pts[rr - 1].append(summa)

        if summa:
            rates[r] = summa
            # separator
            h0 = ht * r + 1
            h03 = h0 - gap
            canvas.create_rectangle(1, h03, width, h0, fill='lightgray', width=1, tag='main')
            h0 += ht
            h03 = h0 - gap
            midis = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
            for idx in range(len(pts[0])):
                x1 = width * pts[0][idx] / summa
                x2 = width * pts[1][idx] / summa
                c = (127 - a[idx]) * 2
                col = '#' + bytearray([c, c, c]).hex()
                note = n[idx]
                canvas.create_rectangle(x1, h0, x2, h0 - note * scale_y, fill=col, width=0, tag='main')

            for idx in range(len(pts[0])):
                x1 = width * pts[0][idx] / summa
                x2 = width * pts[1][idx] / summa
                note = n[idx]
                text = midis[note % 12] + str(note // 12 - 1)
                col = 'white'
                if a[idx] < 32:
                    col = 'black'
                canvas.create_text((x1 + x2) // 2, h03 - 2, text=text, anchor=tk.S, fill=col, tag='main')

        canvas.create_text(6, h0 - ht, text=midi_names[r], anchor=tk.NW, tag='main')


def show_pointer(row):
    gap = 10
    rows = len(note_values) // 4
    width = canvas.winfo_reqwidth() - gap
    height = canvas.winfo_reqheight() - gap
    ht = height // rows
    wd = width // slots
    width = wd * slots
    h0 = ht * (row + 1)
    h03 = h0 - gap
    canvas.delete('pointer' + str(row))
    canvas.create_rectangle(1, h03, width, h0, fill='white', width=1, tag='pointer' + str(row))
    p = pointers[row]
    summa = 0
    r4 = row * 4
    for i in range(p):
        for rr in range(1, 3):
            summa += note_values[r4 + rr][i]
    x = width * summa / rates[row]
    canvas.create_rectangle(x, h03, x + gap, h0, fill='blue', width=1, tag='pointer' + str(row))


def handle_midi():
    global timeline
    global master
    global shifted
    global rates
    global current_slot
    global current_out
    global alt_mode
    global alt_mode2
    global last_clock
    global pause
    global update_diagram

    changed = False  # duration change
    if midiSync is not None:
        msgs = []
        for msg in midiSync.iter_pending():
            msgs.insert(0, msg)  # reverse the order
        for msg in msgs:
            if msg.type == 'clock':
                now = datetime.datetime.now()
                d = now - last_clock
                if d.seconds < 20:
                    timeline = d.seconds + d.microseconds / 1000000
                last_clock = now
            print(msg)

    if midiIn is not None:
        msgs = []
        for msg in midiIn.iter_pending():
            msgs.insert(0, msg)  # reverse the order

        handled = []
        for msg in msgs:
            if msg.type == 'control_change':
                ctl = msg.control
                if ctl in handled:
                    # ignore preceding values from the same channel
                    continue

                handled.append(ctl)

                if ctl < 32:
                    col = (ctl - 16) // 4
                    row = ctl % 4
                    changed = True
                    first = alt_mode  # or ':' in labels[row][col]['text']
                    # labels[row][col]['text'] = str(msg.value)
                    store_value(msg.value, row, col, first, alt_mode2)
                elif ctl < 62:
                    col = (ctl - 46) // 4 + 4
                    row = (ctl - 46) % 4
                    changed = True
                    first = alt_mode  # or ':' in labels[row][col]['text']
                    # labels[row][col]['text'] = str(msg.value)
                    store_value(msg.value, row, col, first, alt_mode2)
                elif msg.control == 62:
                    master = master_range_max - msg.value
                    timeline = timeline_base * master / master_range_min
                else:
                    print(str(msg.control) + ',' + str(msg.channel))
            elif msg.type == 'note_on':
                if msg.note == 1 and alt_mode:  # Pause
                    toggle_pause()
                elif msg.note == 3:  # Alt
                    alt_mode = not alt_mode
                    select_block(current_slot, current_out)
                elif msg.note == 6:  # Alt
                    alt_mode2 = not alt_mode2
                    select_block(current_slot, current_out)
                elif msg.note == 26:  # Down
                    current_out += 1
                    if current_out >= len(note_values) // 4:
                        current_out = 0
                    select_block(current_slot, current_out)
                elif msg.note == 25:  # Up
                    if current_out:
                        current_out -= 1
                    else:
                        current_out = len(note_values) // 4 - 1
                    select_block(current_slot, current_out)
                elif msg.note == 27:  # Right
                    current_slot += 1
                    if current_slot >= slots:
                        current_slot = 0
                    select_block(current_slot, current_out)
                else:
                    shifted = msg.note
                print('shifted', shifted, 'slot', current_slot, 'out', current_out)
            else:
                print(msg)

    if changed:
        if first:
            pause = False
        update_diagram = True

    window.after(5, handle_midi)


def close_callback():
    global window
    global pause
    pause = True
    print('BYE')
    time.sleep(3)

    window.update_idletasks()
    window.destroy()


def on_resize(event):
    global update_diagram
    # resize the canvas 
    canvas.config(width=event.width, height=event.height)
    update_diagram = True


#    print(event)


def create_widgets(num_outs):
    global window
    global canvas

    canvas_height = 700
    width = 1200

    window = tk.Tk()
    window.title('Python Hyper Swing Sequencer')
    window.resizable(True, True)
    window.geometry(str(width) + "x" + str(canvas_height))
    window.protocol("WM_DELETE_WINDOW", close_callback)
    frame = tk.Frame(master=window)
    canvas = tk.Canvas(master=frame, width=width, height=canvas_height, bd=1, bg='black')
    canvas.pack(side=tk.TOP, fill=tk.Y, padx=5, pady=5, ipadx=5, ipady=5, expand=False)
    canvas.bind("<Configure>", on_resize)
    frame.pack()

    m = 0
    c = 0
    for o in range(num_outs):
        n = midiOuts[m].name
        title = n + " ch." + str(channels[m][c])
        c += 1
        if c >= len(channels[m]):
            m += 1
            c = 0

        midi_names.append(title)

        for row in range(4):
            values = []
            for col in range(1, 8 * slots + 1):
                v = random.randint(50, 80)
                values.append(v)
            note_values.append(values)


def stop(out, pitch, velo, channel):
    # print('off', out.name, pitch, velo, channel)
    msg = mido.Message('note_off', note=pitch, velocity=velo, channel=channel)
    out.send(msg)
    # pl['background'] = 'grey'


def to_midi(no):
    midis = {'C': 0, 'D': 2, 'E': 4, 'F': 5, 'G': 7, 'A': 9, 'B': 11}
    if ord(no[0]) in range(ord('0'), ord('9') + 1):
        return int(no)

    num = midis[no[0]]
    p = 1
    if no[p] == '#':
        num += 1
        p += 1
    octave = int(no[p:]) + 1
    num += octave * 12
    return num


def channel_step(row, channel, out):
    global pointers

    if pause:
        return

    step = (pointers[row] % (8 * slots))

    # pl = notes[0 + row * 4][step + 1]
    no = note_values[0 + row * 4][step]
    du = note_values[1 + row * 4][step]  # duration
    pa = note_values[2 + row * 4][step]  # pause
    ve = note_values[3 + row * 4][step]  # velocity

    pitch = no  # to_midi(no) + shifted
    while pitch > 127:
        pitch -= 12

    if ve and du:
        # pl['background'] = 'white'
        msg = mido.Message('note_on', note=pitch, velocity=ve, channel=channel)
        out.send(msg)
        # print('on', pitch, velo, channel)

        # print(no, du, pa, ve)
        # the rates[] corresponds to timeline, sec
        after = int(du / rates[row - 1] * timeline * 1000)
        # this stops the note
        window.after(after, stop, out, pitch, 0, channel)
        show_pointer(row)

    # advance the pointer of this channel
    pointers[row] += 1
    if pointers[row] < 8 * slots:
        after = int((du + pa) / rates[row] * timeline * 1000)
        window.after(after, channel_step, row, channel, out)
    else:
        pointers[row] = 0
        # was this the last channel tp stop?
        for p in pointers:
            if p:  # another channel is running
                return

        window.after(10, step_all)


def step_all():
    if pause:
        window.after(50, step_all)
        return

    i = 0
    for m in range(len(midiOuts)):
        out = midiOuts[m]
        for channel in channels[m]:
            channel_step(i, channel, out)
            i += 1


def keypress(event):
    global pause
    print(event)
    if event.char == ' ':
        toggle_pause()


def update_window2():
    global update_diagram
    window.after(500, update_window2)
    if update_diagram:
        update_diagram = False
        update_rates()


def print_info():
    print('SEND ALL - Synchronize knobs with selected instrument slot')
    print('REC ARM 1 - Toggle Alt mode')
    print('REC ARM 2 - Toggle Alt mode 2')
    print('BANK LEFT/RIGHT - Select channel')
    print('SOLO - Select time slot')


def run():
    num_midis = init_midi()
    num_outs = init_channels(num_midis)
    create_widgets(num_outs)
    print_info()

    window.after(10, handle_midi)
    window.after(10, step_all)
    window.bind('<Key>', keypress)

    window.after(1, update_window2)

    window.mainloop()


if __name__ == "__main__":
    run()
